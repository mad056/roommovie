package com.example.juls.openmoviedatabasegson;

import android.content.DialogInterface;
import android.graphics.Movie;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    //public static JSONArray MovieArray;

    Button searchbotton;
    EditText searchtext;
    TextView title;
    TextView year;
    ImageView yea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = findViewById(R.id.title2);
        searchtext = findViewById(R.id.searchtext);
        year = findViewById(R.id.year);
        searchbotton = findViewById(R.id.gobutton);
        yea = findViewById(R.id.imageView);


    }

    public void SearchClick(View v){
        String oldString = searchtext.getText().toString();
        String newString = oldString.trim();
        MakeRequest(newString);
    }

    // Fck
    public void MakeRequest(String i) {
        RequestQueue queue = Volley.newRequestQueue(this);
        //http://www.omdbapi.com/?t=cars+2&apikey=3545b96
        String apikey  = "&apikey=3545b96";
        String url ="https://www.omdbapi.com/?t=";

        String FormattedString = i.replaceAll(" ", "+");

        String FinalURL = url + FormattedString + apikey;
        System.out.println(FinalURL);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, FinalURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        Gson g = new Gson();

                        Moviee p = g.fromJson(response.toString(), Moviee.class);

                        if (p.Title == null) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Oh no!")
                                    .setMessage("Movie Not Found")
                                    .setCancelable(false)
                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        } else {
                            title.setText("TITLE : " + p.Title);
                            year.setText("YEAR :" + p.Year);
                            Picasso.get().load(p.Poster).into(yea);
                        }
                        //for (int i = 0; i < MovieArray.length(); i++) {

                        //    }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }


                });

// Access the RequestQueue through your singleton class.
        queue.add(jsonObjectRequest);
    }
}
